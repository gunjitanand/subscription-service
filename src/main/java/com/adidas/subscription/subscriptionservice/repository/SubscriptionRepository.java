package com.adidas.subscription.subscriptionservice.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adidas.subscription.subscriptionservice.entity.Subscription;

public interface SubscriptionRepository extends JpaRepository<Subscription, Long> {
	
	 Optional<Subscription> findByEmail(String email);

}
