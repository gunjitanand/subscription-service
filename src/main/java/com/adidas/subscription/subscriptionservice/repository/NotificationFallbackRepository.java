package com.adidas.subscription.subscriptionservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.adidas.subscription.subscriptionservice.entity.NotificationFallback;

public interface NotificationFallbackRepository extends JpaRepository<NotificationFallback, Long> {

	@Query(value = "SELECT * FROM notificationfallback WHERE processed_ind = 'Y' ", nativeQuery = true)
	List<NotificationFallback> findAllUnprocessed();

}
