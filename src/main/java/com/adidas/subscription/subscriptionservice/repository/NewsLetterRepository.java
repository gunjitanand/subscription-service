package com.adidas.subscription.subscriptionservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.adidas.subscription.subscriptionservice.entity.NewsLetter;

@Repository
public interface NewsLetterRepository extends JpaRepository<NewsLetter, Long> {
	
	
	

}
