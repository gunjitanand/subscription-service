package com.adidas.subscription.subscriptionservice.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.adidas.subscription.subscriptionservice.exception.ResourceNotFoundException;
import com.adidas.subscription.subscriptionservice.model.SubscriptionModel;
import com.adidas.subscription.subscriptionservice.model.SubscriptionResponse;
import com.adidas.subscription.subscriptionservice.service.SubscriptionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value="subscriptionplatform", description="supports opeartion related to subscriptions")
@RestController
public class SubscriptionController {

	
	@Autowired
	private SubscriptionService subscriptionService;

	
	@ApiOperation(value = "Creates a subscription for the user having newsletter id", response = Iterable.class)
	@PostMapping("/subscription/create")
	public ResponseEntity<SubscriptionResponse> createUser(@Valid @RequestBody SubscriptionModel subscriptionModel) throws Exception {
		return subscriptionService.createUser(subscriptionModel);
	}

	@ApiOperation(value = "Gets the details of subscription for specific user", response = Iterable.class)
	@GetMapping("/subscription/details/{id}")
	public SubscriptionModel getUser(@PathVariable Long id) throws ResourceNotFoundException {
		return subscriptionService.getDetails(id);

	}

	@ApiOperation(value = "Gets the details of all the users and their subscriptions ", response = Iterable.class)
	@GetMapping("/subscription/all")
	public List<SubscriptionModel> getUsers() {
		return subscriptionService.getAllSubscriptions();
	}

	@ApiOperation(value = "Cancels the subscription of specific user", response = Iterable.class)
	@DeleteMapping("/subscription/cancel/{id}")
	public ResponseEntity<Object> cancelSubscription(@PathVariable Long id) throws ResourceNotFoundException {
		return subscriptionService.cancelSubscription(id);
	}

}
