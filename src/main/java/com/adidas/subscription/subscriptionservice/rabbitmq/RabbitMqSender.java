package com.adidas.subscription.subscriptionservice.rabbitmq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.adidas.subscription.subscriptionservice.entity.Subscription;

@Service
public class RabbitMqSender {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMqSender.class);

	
	@Autowired
	private AmqpTemplate rabbitTemplate;
	
	@Value("${rabbitmq.exchange}")
	private String exchange;
	
	@Value("${rabbitmq.routingkey}")
	private String routingkey;	
	
	public void send(Subscription subscription) {
		rabbitTemplate.convertAndSend(exchange, routingkey, subscription);
		LOGGER.info("message sent to queue " + subscription.getEmail());
		
	    
	}

}
