package com.adidas.subscription.subscriptionservice.rabbitmq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.adidas.subscription.subscriptionservice.entity.NotificationFallback;
import com.adidas.subscription.subscriptionservice.entity.Subscription;
import com.adidas.subscription.subscriptionservice.repository.NotificationFallbackRepository;

@Component
public class RabbitMqReceiver implements RabbitListenerConfigurer {

	private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMqReceiver.class);

	@Autowired
	RestTemplate restTemplate;

	@Value("${notification.service.url}")
	String url;

	@Autowired
	NotificationFallbackRepository notificationFallbackRepository;

	@Override
	public void configureRabbitListeners(RabbitListenerEndpointRegistrar arg0) {
		// TODO Auto-generated method stub

	}

	@RabbitListener(queues = "${rabbitmq.queue}")
	public void receivedMessage(Subscription subscription) {
		LOGGER.info("subscription Received is.. " + subscription.getEmail());
		try {
			restTemplate.postForObject(url, subscription, String.class);
			LOGGER.info("message sent to notification service");
		} catch (RestClientException e1) {

			LOGGER.error("error while sending message to notification service" + e1.getMessage());

			// the failed message is inserted in this fallback table to be
			// picked up by scheduler and sent to queue later

			NotificationFallback fallback = new NotificationFallback();
			fallback.setSubscriptionId(String.valueOf(subscription.getId()));
			fallback.setProcessedInd("N");
			notificationFallbackRepository.save(fallback);

			LOGGER.info("subscription inserted in fallback table.. " + subscription.getEmail());

		}
	}

}
