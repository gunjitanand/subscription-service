package com.adidas.subscription.subscriptionservice.service;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.adidas.subscription.subscriptionservice.entity.NewsLetter;
import com.adidas.subscription.subscriptionservice.entity.NotificationFallback;
import com.adidas.subscription.subscriptionservice.entity.Subscription;
import com.adidas.subscription.subscriptionservice.exception.ResourceExistsException;
import com.adidas.subscription.subscriptionservice.exception.ResourceNotFoundException;
import com.adidas.subscription.subscriptionservice.model.SubscriptionModel;
import com.adidas.subscription.subscriptionservice.model.SubscriptionResponse;
import com.adidas.subscription.subscriptionservice.rabbitmq.RabbitMqSender;
import com.adidas.subscription.subscriptionservice.repository.NewsLetterRepository;
import com.adidas.subscription.subscriptionservice.repository.NotificationFallbackRepository;
import com.adidas.subscription.subscriptionservice.repository.SubscriptionRepository;

@Service
public class SubscriptionService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SubscriptionService.class);

	@Autowired
	SubscriptionRepository subscriptionRepository;

	@Autowired
	RestTemplate restTemplate;

	@Value("${notification.service.url}")
	String url;

	@Autowired
	NewsLetterRepository newsletterRepository;

	@Autowired
	NotificationFallbackRepository notificationFallbackRepository;

	@Autowired
	RabbitMqSender rabbitMqSender;

	/**
	 * 
	 * creates a new subscription
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public ResponseEntity<SubscriptionResponse> createUser(SubscriptionModel model) throws Exception {
		Subscription user = new Subscription();
		if (subscriptionRepository.findByEmail(model.getEmail()).isPresent()) {
			throw new ResourceExistsException("The Email is already Present, Failed to Create new User");
		} else {
			if (newsletterRepository.findById(model.getNewsLetterId()).isPresent()) {
				user.setEmail(model.getEmail());
				user.setFirstname(model.getFirstName());
				user.setConsentflag(model.getConsentFlag());
				user.setGender(model.getGender());
				NewsLetter newsLetter = new NewsLetter();
				newsLetter.setId(model.getNewsLetterId());
				user.setNewsLetter(newsLetter);

				Subscription savedUser = subscriptionRepository.save(user);

				return sendNotification(savedUser);

			} else {
				throw new ResourceNotFoundException("NewsLetter does not exists!");
			}
		}
	}

	/**
	 * 
	 * asynchronously sends the message to queue 
	 * 
	 * @param savedUser
	 * @return
	 * @throws Exception
	 */
	public ResponseEntity<SubscriptionResponse> sendNotification(Subscription savedUser) throws Exception {

		try {
			rabbitMqSender.send(savedUser);
		} catch (Exception e) {
			LOGGER.error("Error while sending msg to queue " + e.getMessage());
			// the failed message is inserted in this fallback table to be
			// picked up by scheduler and sent queue later

			NotificationFallback fallback = new NotificationFallback();
			fallback.setSubscriptionId(String.valueOf(savedUser.getId()));
			fallback.setProcessedInd("N");
			notificationFallbackRepository.save(fallback);
		}

		SubscriptionResponse response = new SubscriptionResponse();
		response.setSubscriptionId(String.valueOf(savedUser.getId()));
		return ResponseEntity.ok(response);

	}

	/**
	 * cancels the existing subscription for a particular subscriber
	 * 
	 * @param id
	 * @return
	 * @throws ResourceNotFoundException
	 */
	public ResponseEntity<Object> cancelSubscription(Long id) throws ResourceNotFoundException {
		if (subscriptionRepository.findById(id).isPresent()) {
			subscriptionRepository.deleteById(id);
			return ResponseEntity.ok().body("Successfully deleted specified record");
		} else {
			throw new ResourceNotFoundException("Resource not found : " + String.valueOf(id));
		}
	}

	/**
	 * gets the details of subscriptions for a particular subscription id
	 * 
	 * @param id
	 * @return
	 * @throws ResourceNotFoundException
	 */
	public SubscriptionModel getDetails(Long id) throws ResourceNotFoundException {
		if (subscriptionRepository.findById(id).isPresent()) {
			Subscription user = subscriptionRepository.findById(id).get();
			SubscriptionModel subscriptionModel = new SubscriptionModel();
			subscriptionModel.setEmail(user.getEmail());
			subscriptionModel.setFirstName(user.getFirstname());
			subscriptionModel.setGender(user.getGender());
			subscriptionModel.setConsentFlag(user.getConsentflag());
			subscriptionModel.setNewsLetterId(user.getNewsLetter().getId());
			return subscriptionModel;
		} else
			throw new ResourceNotFoundException("Resource not found : " + String.valueOf(id));
	}

	/**
	 * gets all the subscriptions present in the system
	 * 
	 * @return
	 */
	public List<SubscriptionModel> getAllSubscriptions() {
		List<Subscription> users = subscriptionRepository.findAll();
		return users.stream().map(x -> {
			SubscriptionModel model = new SubscriptionModel();
			model.setEmail(x.getEmail());
			model.setFirstName(x.getFirstname());
			model.setGender(x.getGender());
			model.setConsentFlag(x.getConsentflag());
			model.setNewsLetterId(x.getNewsLetter().getId());
			return model;
		}).collect(Collectors.toList());
	}

}
