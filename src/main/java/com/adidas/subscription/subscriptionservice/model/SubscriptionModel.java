package com.adidas.subscription.subscriptionservice.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class SubscriptionModel {
	
	
	@Email(message = "Please enter valid email", regexp="^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\\.[a-zA-Z.]{2,5}")
	@NotNull(message = "Please enter email")
	private String email;
	@Size(min=4, message = "Name should be atleast 4 characters")
	@Size(max=10, message = "Name should not be greater than 10 characters")
	private String firstName;
	@Pattern(regexp = "^[M|F]{1}$", message ="Gender Must be M or F")
	private String gender;
	@NotNull(message = "Please enter consent flag")
	private String consentFlag;
	@NotNull(message = "Please enter news letter id")
	private Long newsLetterId;
	
	
	
	
	public Long getNewsLetterId() {
		return newsLetterId;
	}
	public void setNewsLetterId(Long newsLetterId) {
		this.newsLetterId = newsLetterId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getConsentFlag() {
		return consentFlag;
	}
	public void setConsentFlag(String consentFlag) {
		this.consentFlag = consentFlag;
	}
//	public List<NewsLetterModel> getNewsLetters() {
//		return newsLetters;
//	}
//	public void setNewsLetters(List<NewsLetterModel> newsLetters) {
//		this.newsLetters = newsLetters;
//	}
	
	

}
