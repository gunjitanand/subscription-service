package com.adidas.subscription.subscriptionservice.model;

public class SubscriptionResponse {
	
	private String subscriptionId;

	public String getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}
	
	
	

}
