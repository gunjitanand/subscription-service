package com.adidas.subscription.subscriptionservice.exception;

public class ResourceExistsException extends Exception {

	private static final long serialVersionUID = -9079454849611061075L;

	public ResourceExistsException() {
		super();
	}

	public ResourceExistsException(final String message) {
		super(message);
	}

}
