package com.adidas.subscription.subscriptionservice.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.adidas.subscription.subscriptionservice.entity.NewsLetter;
import com.adidas.subscription.subscriptionservice.entity.Subscription;
import com.adidas.subscription.subscriptionservice.exception.ResourceExistsException;
import com.adidas.subscription.subscriptionservice.exception.ResourceNotFoundException;
import com.adidas.subscription.subscriptionservice.model.SubscriptionModel;
import com.adidas.subscription.subscriptionservice.model.SubscriptionResponse;
import com.adidas.subscription.subscriptionservice.rabbitmq.RabbitMqSender;
import com.adidas.subscription.subscriptionservice.repository.NewsLetterRepository;
import com.adidas.subscription.subscriptionservice.repository.NotificationFallbackRepository;
import com.adidas.subscription.subscriptionservice.repository.SubscriptionRepository;

@SpringBootTest
public class SubscriptionServiceTest {

	@InjectMocks
	SubscriptionService subscriptionService;

	@Mock
	SubscriptionRepository subscriptionRepository;

	@Mock
	RestTemplate restTemplate;

	@Value("${notification.service.url}")
	String url;

	@Mock
	NewsLetterRepository newsletterRepository;

	@Mock
	NotificationFallbackRepository notificationFallbackRepository;

	@Mock
	RabbitMqSender rabbitMqSender;

	@Mock
	AmqpTemplate rabbitTemplate;

	@Test()
	public void createUserEmailExistsTest() throws Exception {

		Subscription user = new Subscription();
		SubscriptionModel model = new SubscriptionModel();
		model.setEmail("abcd@gmail.coom");

		Optional<Subscription> userOp = Optional.of(user);

		Mockito.doReturn(userOp).when(subscriptionRepository).findByEmail("abcd@gmail.coom");

		Throwable thrown = catchThrowable(() -> subscriptionService.createUser(model));

		assertThat(thrown).isInstanceOf(ResourceExistsException.class);

	}

	@Test()
	public void createUserEmailDoesNotExistsTest() throws Exception {

		Subscription user = new Subscription();
		user.setId(1L);
		SubscriptionModel model = new SubscriptionModel();
		model.setEmail("abcd@gmail.coom");
		model.setNewsLetterId(2L);

		NewsLetter newsLetter = new NewsLetter();

		Optional<Subscription> userOp = Optional.empty();
		Optional<NewsLetter> newsLetterOp = Optional.of(newsLetter);

		Mockito.doReturn(userOp).when(subscriptionRepository).findByEmail("abcd@gmail.coom");
		Mockito.doReturn(newsLetterOp).when(newsletterRepository).findById(2L);
		Mockito.doReturn(user).when(subscriptionRepository).save(Mockito.any(Subscription.class));

		ResponseEntity<SubscriptionResponse> responseEntity = subscriptionService.createUser(model);

		assertThat(responseEntity.getBody().getSubscriptionId()).isEqualTo("1");

	}

	@Test()
	public void createUserNewsletterDoesNotExistsTest() throws Exception {

		Subscription user = new Subscription();
		user.setId(1L);
		SubscriptionModel model = new SubscriptionModel();
		model.setEmail("abcd@gmail.coom");

		Optional<Subscription> userOp = Optional.empty();

		Mockito.doReturn(userOp).when(subscriptionRepository).findByEmail("abcd@gmail.coom");

		Throwable thrown = catchThrowable(() -> subscriptionService.createUser(model));

		assertThat(thrown).isInstanceOf(ResourceNotFoundException.class);

	}

	@Test
	public void sendNotificationFallbackTest() throws Exception {
		Subscription savedUser = new Subscription();
		savedUser.setId(1L);

		ResponseEntity<SubscriptionResponse> responseEntity = subscriptionService.sendNotification(savedUser);

		assertThat(responseEntity.getBody().getSubscriptionId()).isEqualTo("1");
	}

	@Test()
	public void cancelSubscriptionResourceNotPresentTest() throws ResourceNotFoundException {
		Optional<Subscription> userOp = Optional.empty();

		Mockito.doReturn(userOp).when(subscriptionRepository).findById(2L);

		Throwable thrown = catchThrowable(() -> subscriptionService.cancelSubscription(2L));

		assertThat(thrown).isInstanceOf(ResourceNotFoundException.class);
	}

	@Test()
	public void cancelSubscriptionResourcePresentTest() throws ResourceNotFoundException {
		Subscription user = new Subscription();
		user.setId(2L);
		Optional<Subscription> userOp = Optional.of(user);

		Mockito.doReturn(userOp).when(subscriptionRepository).findById(2L);

		ResponseEntity<Object> response = subscriptionService.cancelSubscription(2L);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
	}

	@Test()
	public void getDetailsResourceNotFoundExceptionTest() throws ResourceNotFoundException {

		Optional<Subscription> userOp = Optional.empty();

		Mockito.doReturn(userOp).when(subscriptionRepository).findById(2L);

		Throwable thrown = catchThrowable(() -> subscriptionService.getDetails(2l));

		assertThat(thrown).isInstanceOf(ResourceNotFoundException.class);
	}

	@Test()
	public void getDetailsTest() throws ResourceNotFoundException {

		Subscription user = new Subscription();
		NewsLetter letter = new NewsLetter();
		letter.setId(2L);
		user.setNewsLetter(letter);
		user.setEmail("abcd@gmail.coom");
		SubscriptionModel model = new SubscriptionModel();
		model.setEmail("abcd@gmail.coom");
		model.setNewsLetterId(2L);

		Optional<Subscription> userOp = Optional.of(user);

		Mockito.doReturn(userOp).when(subscriptionRepository).findById(2L);
		subscriptionService.getDetails(2l);

		SubscriptionModel modelResponse = subscriptionService.getDetails(2l);
		assertThat(modelResponse.getEmail()).isEqualTo("abcd@gmail.coom");

	}

	@Test()
	public void getAllDetailsTest() throws ResourceNotFoundException {

		Subscription user1 = new Subscription();
		NewsLetter letter = new NewsLetter();
		letter.setId(2L);
		user1.setNewsLetter(letter);
		user1.setEmail("abcd@gmail.coom");
		SubscriptionModel model = new SubscriptionModel();
		model.setEmail("abcd@gmail.coom");
		model.setNewsLetterId(2L);

		Subscription user2 = new Subscription();
		NewsLetter letter2 = new NewsLetter();
		letter2.setId(2L);
		user2.setNewsLetter(letter2);
		user2.setEmail("abcd@gmail.coom");
		SubscriptionModel model2 = new SubscriptionModel();
		model2.setEmail("abcd@gmail.coom");
		model2.setNewsLetterId(2L);

		List<Subscription> subList = new ArrayList<>();
		subList.add(user1);
		subList.add(user2);

		Mockito.doReturn(subList).when(subscriptionRepository).findAll();

		List<SubscriptionModel> modelResponse = subscriptionService.getAllSubscriptions();
		assertThat(modelResponse.size()).isEqualTo(2);
	}

}
